use actix_web::{get, App, HttpServer, Responder};

#[get("/")]
async fn index() -> impl Responder {
    "Welcome"
}

#[get("/home")]
async fn home() -> impl Responder {
    "home page"
}

#[get("/profile")]
async fn profile() -> impl Responder {
    "profile"
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(index)
            .service(home)
            .service(profile)
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}