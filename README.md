# Individual Project 2 

This project implements Simple REST API/web service in Rust, a Dockerfile to containerize service and a CI/CD pipeline files.

## Rust Functionality
This Rust microservice utilizes the Actix web framework to provide a simple REST API, offering three endpoints. 
1. `/` index page 
2. `/home ` home page
3. `/profile` profile page

run `cargo run` to test the functionality: \
![](1.png) \
![](2.png) \
![](3.png) \

## Docker Configuration
1. Write the Dockerfile 

2. run `docker build -t individual2 .` to build \
![](4.png)

3. run `docker run individual2` to run docker image \
![](5.png)

4. Then, the application should be able to be accessed on http://localhost:8088/.

## CI/CD Pipeline
1. Write the `.gitlab-ci.yml` file 
![](6.png)


Demo video available [here]().
